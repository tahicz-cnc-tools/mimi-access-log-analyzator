# mimi-access-log-analyzator

## RUN

Base command `php analyse.php analyse:quantum --log-file=LOG_FILE_PATH --target-file=TARGET_FILE_PATH`

### Quantum  analyze

* `--by` - make quantum analyse by `request-url` or by `remote-host` (default)
* `--limit` - get first N rows
