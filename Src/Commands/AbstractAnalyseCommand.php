<?php

namespace App\Commands;

use App\Data\Row;
use Generator;
use Monolog\Logger;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Filesystem\Filesystem;

abstract class AbstractAnalyseCommand extends Command
{
	protected const ARG_LOG_FILE = 'log-file';
	protected const ARG_TARGET_FILE = 'target-file';
	/**
	 * @var Logger
	 */
	protected $logger;

	abstract protected function doSave($file, array $rows);

	public function __construct(Logger $logger)
	{
		parent::__construct(null);
		$this->logger = $logger;
	}

	protected function configure()
	{
		$this->addOption(self::ARG_LOG_FILE, 's', InputOption::VALUE_REQUIRED, 'Log soubor pro analýzu')
			->addOption(self::ARG_TARGET_FILE, 't', InputOption::VALUE_REQUIRED, 'Cílový soubor pro  výstup');
	}

	/**
	 * @return Row[]
	 */
	protected function getFileContent(string $filePath): array
	{
		$fileContent = [];
		foreach ($this->loadFile($filePath) as $line) {
			$fileContent[] = $this->parseLine($line);
		}

		return $fileContent;
	}

	/**
	 * @return Generator
	 */
	private function loadFile(string $filePath): Generator
	{
		$filesystem = new Filesystem();
		if (!$filesystem->exists($filePath)) {
			throw new RuntimeException("Načítaný soubor neexistuje - {$filePath}");
		}
		$file = fopen($filePath, "r") or die("Couldn't get handle");
		$buffer = [];
		if ($file) {
			while (($line = fgets($file)) !== false) {
				yield $line;
			}
			fclose($file);
		}

		return $buffer;
	}

	/**
	 * 37.48.4.173 - - [25/Jan/2021:06:25:31 +0100] "GET /skripty/Sticky/sticky-kit.min.js HTTP/1.1" 200 5294 "https://www.mimibazar.cz/" "Mozilla/5.0 (Linux; Android 8.0.0; SM-A600FN) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Mobile Safari/537.36" www.mimibazar.cz:443 [www.mimibazar.cz] 10621 1312 51420 + "-" "-" 192.168.30.150
	 */
	private function parseLine(string $row): ?Row
	{
		switch (preg_match(
			'~([\d\.]{7,15}|-) - (.+) \[([0123]?[0-9]/[a-z]{3}/[1-9]?[0-9]{3}:[0-9]{2}:[0-9]{2}:[0-9]{2} \+\d{4})\] \"([A-Z]{3,8}) (.+) HTTPS?/\d\.\d\" (\d\d{2}) (\d+) "(.*)" "(.+)" (www\.mimi.{5,6}\.(?:cz|sk)):(\d{2,8}) \[(www\.mimi.{5,6}\.(?:cz|sk))\] (\d+) (\d+) (\d+) (.) "(.+)" "(.+)" ([\d\.]{7,15})~i',
			$row,
			$matches
		)) {
			case 0:
				$this->logger->warning("Řádek nebyl řádně zparsován: {$row}");
				$newRow = null;
				break;
			case 1:
				$newRow = new Row(
					$matches[1],
					$matches[3],
					$matches[4],
					$matches[5],
					$matches[6],
					$matches[7],
					$matches[8],
					$matches[9],
					$matches[19]
				);
				break;
			case false:
				$this->logger->error("REGEX nezafunoval");
				$newRow = null;
				break;
			default:
				$newRow = null;
				break;
		}

		return $newRow;
	}

	protected function saveData(string $targetFile, array $rows, ?string $title = null): void
	{
		$file = fopen('output/' . $targetFile, 'w');
		if (!empty($title)) {
			$titleLength = strlen($title) + 4;
			if ($titleLength % 2 === 1) {
				$titleLength++;
			}
			fputs($file, str_pad('', $titleLength, '#', STR_PAD_BOTH) . PHP_EOL);
			fputs($file, str_pad(' ' . $title . ' ', $titleLength, '#', STR_PAD_BOTH) . PHP_EOL);
			fputs($file, str_pad('', $titleLength, '#', STR_PAD_BOTH) . PHP_EOL . PHP_EOL);
		}

		$this->doSave($file, $rows);

		fclose($file);
	}
}
