<?php

namespace App\Commands;

use Monolog\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TimeAnalyseCommand extends AbstractAnalyseCommand
{
	private const ARG_LIMIT = 'limit';
	protected static $defaultName = 'analyse:time';
	/**
	 * @var InputInterface
	 */
	private $input;
	/**
	 * @var OutputInterface
	 */
	private $output;

	public function __construct(Logger $logger)
	{
		parent::__construct($logger);
	}

	protected function configure()
	{
		parent::configure();
		$this->addOption(self::ARG_LIMIT, 'l', InputOption::VALUE_OPTIONAL, 'Limit na avýstupu.')
			->setDescription('Spočítá nejčetnější záznamy podle access logu.');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->input = $input;
		$this->output = $output;
		$this->output->write("Načítám log soubor '{$this->input->getOption(self::ARG_LOG_FILE)}'.", true, OutputInterface::OUTPUT_NORMAL);
		$fileContent = $this->getFileContent($this->input->getOption(self::ARG_LOG_FILE));
		$outputData = [];

		$this->output->write("Zpracovávám načtená data.", true, OutputInterface::OUTPUT_NORMAL);
		foreach ($fileContent as $item) {
			if (is_null($item)) {
				continue;
			}
			$outputData[$item->getDatetime()->getTimestamp()][$item->getUrl()][] = [
				'remote-host' => $item->getRemoteHost(),
				'referer' => $item->getReferer(),
				'user-agent' => $item->getUserAgent(),
			];
			if (!key_exists('count', $outputData[$item->getDatetime()->getTimestamp()][$item->getUrl()])) {
				$outputData[$item->getDatetime()->getTimestamp()][$item->getUrl()]['count'] = 0;
				$outputData[$item->getDatetime()->getTimestamp()][$item->getUrl()]['datetime'] = $item->getDatetime()->format('j.n.Y H:i:s');
			}
			$outputData[$item->getDatetime()->getTimestamp()][$item->getUrl()]['count']++;
		}

		$this->output->write("Ukládám data.", true, OutputInterface::OUTPUT_NORMAL);
		$this->saveData(
			$this->input->getOption(self::ARG_TARGET_FILE),
			$outputData,
			'Time analyse'
		);

		return 0;
	}

	protected function doSave($file, array $rows)
	{
		if (empty($this->input->getOption(self::ARG_LIMIT))) {
			$output = $rows;
		} else {
			foreach ($rows as $key => $records) {
				$rows[$key]['count'] = $this->countTimes($records);
			}
			uasort($rows, [$this, 'sortRecordsDesc']);
			$output = array_slice($rows, 0, $this->input->getOption(self::ARG_LIMIT), true);
		}

		fputs($file, json_encode($output));
	}

	private function countTimes(array $records): int
	{
		$totalCount = 0;
		foreach ($records as $record) {
			$totalCount += $record['count'];
		}

		return $totalCount;
	}

	private function sortRecordsDesc(array $a, array $b)
	{
		return $b['count'] <=> $a['count'];
	}
}
