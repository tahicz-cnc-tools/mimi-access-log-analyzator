<?php

namespace App\Commands;

use Monolog\Logger;
use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class QuantumAnalyseCommand extends AbstractAnalyseCommand
{
	private const ARG_STAT_BY = 'by';
	private const ARG_LIMIT = 'limit';
	private const VAL_STAT_BY_REMOTE_HOST = 'remote-host';
	private const VAL_STAT_BY_REQUESTED_URL = 'requested-url';
	private const VAL_STAT_BY = [
		self::VAL_STAT_BY_REMOTE_HOST,
		self::VAL_STAT_BY_REQUESTED_URL,
	];
	protected static $defaultName = 'analyse:quantum';
	/**
	 * @var InputInterface
	 */
	private $input;
	/**
	 * @var OutputInterface
	 */
	private $output;

	public function __construct(Logger $logger)
	{
		parent::__construct($logger);
	}

	protected function configure()
	{
		parent::configure();
		$this->addOption(
			self::ARG_STAT_BY,
			'o',
			InputOption::VALUE_OPTIONAL,
			'Podle čeho počítat (' . implode(',', self::VAL_STAT_BY) . ')',
			self::VAL_STAT_BY_REMOTE_HOST
		)
			->addOption(self::ARG_LIMIT, 'l', InputOption::VALUE_OPTIONAL, 'Limit na avýstupu.')
			->setDescription('Spočítá nejčetnější záznamy podle access logu.');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->input = $input;
		$this->output = $output;
		if (!in_array($this->input->getOption(self::ARG_STAT_BY), self::VAL_STAT_BY)) {
			throw new RuntimeException(
				'Neplatná volba parametru "' . self::ARG_STAT_BY . '". Možné volby jsou ' . implode(
					',',
					self::VAL_STAT_BY
				)
			);
		}
		$this->output->write("Načítám log soubor '{$this->input->getOption(self::ARG_LOG_FILE)}'.", true, OutputInterface::OUTPUT_NORMAL);
		$fileContent = $this->getFileContent($this->input->getOption(self::ARG_LOG_FILE));
		$outputData = [];
		$this->output->write("Zpracovávám načtená data.", true, OutputInterface::OUTPUT_NORMAL);
		foreach ($fileContent as $row) {
			if (is_null($row)) {
				continue;
			}
			switch ($this->input->getOption(self::ARG_STAT_BY)) {
				case self::VAL_STAT_BY_REMOTE_HOST:
					$outputData[$row->getRemoteHost()][] = $row;
					break;
				case self::VAL_STAT_BY_REQUESTED_URL:
					$outputData[$row->getUrl()][] = $row;
					break;
			}
		}

		$this->output->write("Ukládám data.", true, OutputInterface::OUTPUT_NORMAL);
		$this->saveData(
			$this->input->getOption(self::ARG_TARGET_FILE),
			$outputData,
			'Quantum by ' . $this->input->getOption(self::ARG_STAT_BY)
		);

		return 0;
	}

	protected function doSave($file, array $rows)
	{
		arsort($rows);
		if (empty($this->input->getOption(self::ARG_LIMIT))) {
			$limit = PHP_INT_MAX;
		} else {
			$limit = $this->input->getOption(self::ARG_LIMIT);
		}
		$i = 0;
		foreach ($rows as $key => $row) {
			$count = count($row);
			fputs($file, "{$key} => {$count}" . PHP_EOL);
			$i++;
			if ($i >= $limit) {
				break;
			}
		}
	}
}
