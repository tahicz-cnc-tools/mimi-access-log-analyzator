<?php
namespace App\Data;

use DateTimeImmutable;
use DateTimeInterface;

class Row
{
	/**
	 * @var string
	 */
	private $remoteHost;
	/**
	 * @var DateTimeImmutable
	 */
	private $datetime;
	/**
	 * @var string
	 */
	private $method;
	/**
	 * @var string
	 */
	private $url;
	/**
	 * @var int
	 */
	private $statusCode;
	/**
	 * @var int
	 */
	private $dataSize;
	/**
	 * @var string
	 */
	private $referer;
	/**
	 * @var string
	 */
	private $userAgent;
	/**
	 * @var string
	 */
	private $lbip;

	public function __construct(
		string $remoteHost,
		string $datetime,
		string $method,
		string $url,
		int $statusCode,
		int $dataSize,
		string $referer,
		string $userAgent,
		string $lbip
	) {
		$this->remoteHost = $remoteHost;
		$this->method = $method;
		$this->url = $url;
		$this->statusCode = $statusCode;
		$this->dataSize = $dataSize;
		$this->referer = $referer;
		$this->userAgent = $userAgent;
		$this->lbip = $lbip;
		$this->datetime = (new DateTimeImmutable())->setTimestamp(strtotime($datetime));
	}

	/**
	 * @return string
	 */
	public function getRemoteHost(): string
	{
		return $this->remoteHost;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function getDatetime(): DateTimeInterface
	{
		return $this->datetime;
	}

	/**
	 * @return string
	 */
	public function getMethod(): string
	{
		return $this->method;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @return int
	 */
	public function getStatusCode(): int
	{
		return $this->statusCode;
	}

	/**
	 * @return int
	 */
	public function getDataSize(): int
	{
		return $this->dataSize;
	}

	/**
	 * @return string
	 */
	public function getReferer(): string
	{
		return $this->referer;
	}

	/**
	 * @return string
	 */
	public function getUserAgent(): string
	{
		return $this->userAgent;
	}

	/**
	 * @return string
	 */
	public function getLbip(): string
	{
		return $this->lbip;
	}
}
