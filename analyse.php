<?php

use App\Commands\QuantumAnalyseCommand;
use App\Commands\TimeAnalyseCommand;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Console\Application;

require_once 'vendor/autoload.php';

$application = new Application();

$quantumLogger = new Logger('quantumLogger');
setLogger($quantumLogger);
$quantumCommand = new QuantumAnalyseCommand($quantumLogger);

$timeAnalyseLogger = new Logger('timeAnalyseLogger');
setLogger($timeAnalyseLogger);
$timeAnalyseCommand = new TimeAnalyseCommand($timeAnalyseLogger);

$application->addCommands([$quantumCommand, $timeAnalyseCommand]);
$application->run();

function setLogger(Logger $logger):void{
	$logger->pushHandler(
		new StreamHandler(__DIR__.'/logs/'.str_replace('Logger','',$logger->getName()).'Log.'.date('YnjHis').'.log',Logger::DEBUG)
	);
}
